# Cahier des Charges

## Introduction

### Présentation du groupe

La convergence du paprika regroupe des étudiants du campus parisien du groupe Ynov afin de travailler sur des projets scolaires ou d’en monter de nouveaux afin de pouvoir s’améliorer tous ensemble et de manière cohérente.

### Nom du groupe

L’origine du nom de ce groupe change d’un projet à l’autre. Le nom seul n’a pas d’importance, seul les actions comptent.

### Logo

![Comment appelle-t-on un rongeur de l’espace ? Un hamsteroïde.](logo.jpg)

### Membres

#### Charles Leroy

Chef de projet, développeur et étudiant.

#### Raphaël Oester

Développeur et étudiant.

<!--PROJECT-->

## Spécificités Fonctionnelles
<!--user_stories-->

## Spécificités Techniques
<!--user_stories-->

## Conclusion

Ce projet semble contraignant à la vue de sa difficulté et des dead-lines imposées, il est nécessaire de prendre des précautions et de faire des retours réguliers afin d'éviter les rendus de dernières minutes.

- Le projet est disponible sur le repo suivant: [https://gitlab.com/leroycharles/uf_infra_2021](https://gitlab.com/leroycharles/uf_infra_2021).
- Le site du projet est disponible à l'adresse suivante: [https://leroycharles.gitlab.io/uf_infra_2021](https://leroycharles.gitlab.io/uf_infra_2021).
- Les releases du projet sont disponibles à l'adresse suivante: [https://gitlab.com/leroycharles/uf_infra_2021/-/releases](https://gitlab.com/leroycharles/uf_infra_2021/-/releases).

### Rendu


### Releases
#### Gestion de Projet
**Version:** 0.0
**But:** Rendu initial permettant la publication du cahier des charges, le lancement de l'intégration continue, la définition du planning, etc
**Date de sortie:** 02/03/2021

#### Prima
**Version:** 0.1
**But:** DockerFile fonctionnels, les images sont accessbiles depuis un reverse proxy, le tout sur un seul serveur physique.
**Date de sortie:** 16/04/2021

#### Duo
**Version:** 0.2
**But:** Loadbalancing fonctionnel, le réseau entreprise est en place, possibilité d'implémenter le réseau de secours
**Date de sortie:** 07/05/2021

#### Alpha
**Version:** 1.0
**But:** Réseau entreprise gérant un réseau de secours au moins, les documents sont prêts pour la présentation
**Date de sortie:** 21/05/2021
