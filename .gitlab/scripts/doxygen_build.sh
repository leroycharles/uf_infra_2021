#!/bin/bash
cp .gitlab/Doxyfile Doxyfile
cp .gitlab/doxygen-bash.sed doxygen-bash.sed
doxygen Doxyfile
rm Doxyfile
rm doxygen-bash.sed