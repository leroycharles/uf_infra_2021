#!/bin/bash
cd docs/presentation

pdflatex main.tex
pdflatex main.tex
pdflatex main.tex

cd ../..
cp docs/presentation/main.pdf Presentation.pdf
rm -rf docs/presentation/*