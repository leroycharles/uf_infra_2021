#!/bin/bash
cp docs/specs.md specs.md
cp docs/logo.jpg logo.jpg
ls -f docs/user_stories/ | while read -r file; do cat docs/user_stories/$file >> us.buffer; done
ls -f docs/user_stories/ | while read -r file; do cat docs/user_stories/$file >>fct.buffer; done
sed -i -e "/<!--PROJECT-->/r README.md" specs.md
sed -i -e "/<!--user_stories-->/r us.buffer" specs.md
sed -i -e "/<!--fct_specs-->/r fct.buffer" specs.md
rm us.buffer