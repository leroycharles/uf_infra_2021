## MultiPass

### Présentation

#### Origine

Ce projet nous a été confier dans le cadre d'une évaluation de nos compétences des modules Infrastructure & SI vues au cours de l'année.

#### Nature

Ce projet prend la forme d'un ensemble de site internet (WordPress, Wekan, Blogango) accessible depuis plusieurs serveurs de manière sécurisée.

#### But et intérêt

Ce projet permet une gestion des serveurs simple et automatique pour le déploiement et l'utilisation de divers applications web.

#### Position dans le secteur

Ce projet est un projet scolaire, il n’a pas vocation à être mis en compétition ou sur un marché.

### Fonctionnel

#### Rappel Contextuel

Ce projet permet une gestion des serveurs simple et automatique pour le déploiement et l'utilisation de divers applications web.


#### Fonctionnalités

Ce site possède 3 applications Web:

- WordPress;
- Wekan;
- Blogango.

Il met en service 3 serveurs:

- 1 serveur "point d'entrée", avec le loadbalancing;
- 2 serveurs "services"  distants, implémentant différents conteneur accesible avec leur reverse proxy.

Ces serveurs sont présent au sein de 2 réseaux différents:

- le réseau "entreprise" contenant le point d'entrée et un serveur service;
- le réseau "secondaire" contenant un point d'entrée.

### Aspect Techniques et Méthodologique

#### Moyens Matériels

Le développement s’effectuera avec la suite de logiciel JetBrains sous licence étudiante.
Le serveur git sera hébergé sur la plate-forme Gitlab.com avec les fonctionnalités gratuites, le GitFLow sera gérer par des plugins intégrés aux logiciels JetBrains et grâce à la version gratuite de GitKrakenGUI. Les images Docker seront publiées sur Docker Hub.

#### Moyens Intellectuels

Afin de pouvoir mener à bien ce projet, nous avons utilisé nos notes des cours suivis à Ynov ainsi que les notes des cours de Linkedin Learning "Découvrir Docker" et "Docker pour les développeurs". De nombreux tutoriels ont été nécessaires.

#### Aspects Économique

Ceci est un projet étudiant, il ne possède pas de financement, le budget est donc de 0€.
